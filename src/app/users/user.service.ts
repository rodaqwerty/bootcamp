import { Injectable } from '@angular/core';
import {HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from './models/models';


@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(private _httpclient: HttpClient) { }

  getusers():Observable<User[]> {

    return this._httpclient.get<User[]>("https://jsonplaceholder.typicode.com/users")

  }

}
