import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListComponent } from './list/list.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { UtilsModule} from "../utils/utils.module"

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    HttpClientModule,
    UtilsModule
  ],

  providers:[
    UserService
  ]

})
export class UsersModule { }
