import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatSliderModule } from "@angular/material/slider";
import { MatTableModule} from "@angular/material/table";


const modules=[
    MatSliderModule,
    MatTableModule,
   
]
@NgModule({
    declarations: [],
    imports: [
    CommonModule,
    ...modules,
    ],
    exports:[
        ...modules
    ]
})
export class UtilsModule{


}
